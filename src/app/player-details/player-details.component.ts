import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.css']
})
export class PlayerDetailsComponent implements OnInit {

  constructor(private route:ActivatedRoute,private ApiService:ApiService) { }
nId;
playerDetails;
  ngOnInit() {
    this.nId=this.route.snapshot.paramMap.get('id');
    
    
    this.playerDetails=this.ApiService.getDetails(this.nId);
    console.log(this.playerDetails);
  }

}
