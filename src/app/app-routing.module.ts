import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlayerListComponent} from './player-list/player-list.component';
import {PlayerDetailsComponent} from './player-details/player-details.component';

const routes: Routes = [
  //{ path:'/', component: PlayerListComponent },
  { path:'playersList', component: PlayerListComponent },
  { path:'playerDetails/:id', component: PlayerDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
