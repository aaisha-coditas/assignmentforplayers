import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';
import { Players } from '../players';
import { Router } from '@angular/router';


@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

  constructor(private ApiService:ApiService,private router: Router) { }


  players;
  

  ngOnInit() {
    this.players=this.ApiService.getPlayers();
  }
  getDetails(id){
    this.router.navigate(['/playersDetails/'+id]);
  }

}
